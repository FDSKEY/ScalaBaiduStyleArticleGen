# ScalaBaiduStyleArticleGen

## 介绍

生成百度体文章，原创不易 安装教程

```shell script
git clone https://gitee.com/FDSKEY/ScalaBaiduStyleArticleGen
cd ScalaBaiduStyleArticleGen
sbt run
```

或者到版本页面下载
## 使用说明

### 1.方法1

```shell script
cd ScalaBaiduStyleArticleGen
scala ./target/scala-2.13/scalabaidustylegen_2.13-0.1.jar
```
### 2.方法2

使用IntelliJ IDEA 2020.1 (Community Edition)打开，使用构建工具编译。

### 4.用IDEA的项目结构工具导入jar包,或者用构建工具（如sbt，gradle）添加jar包依赖。

如：
```scala
import articlemaker.News
import articlemaker.General
import articlemaker.General2
object LeanTest {
  def main(args: Array[String]): Unit = {
    val x = News("百度app","被约谈","xx新闻")
    val y = General("百度app被约谈","xx新闻")
    val z = General2("百度app被约谈","xx新闻")
    List(x,y,z).foreach(i => i.make())
  }
}
```

## 效果

为什么百度app会被约谈呢？结果令人震惊！ Hi,大家好，这里是xx新闻。今天，我们来说一下百度app被约谈 这件事。百度app被约谈是怎么回事呢？百度app相信大家都很熟悉， 但是百度app被约谈这件事究竟是怎么回事呢？可能有人不理解， 为什么百度app会被约谈呢？所以，下面就让小编带大家一起了解吧 百度app被约谈这件事吧，百度app被约谈其实就是百度app被约谈了。 那么百度app为什么会被约谈呢，相信大家都很好奇是怎么回事。大家可能会感到很惊讶， 百度app怎么会被约谈呢？
百度app被约谈就是百度app被约谈啦，这是因为百度app被约谈就是百度app被约谈 就是小编也感到非常惊讶。那么这就是关于百度app被约谈的事情了，大家有没有觉得很神奇呢？ 感谢大家阅读小编的文章，谢谢啦。 如果大家有什么想法和评论，欢迎在文章结尾下方留言。 参与贡献.
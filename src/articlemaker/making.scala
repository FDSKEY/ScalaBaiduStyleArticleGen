package articlemaker
object making {
  def maker():Unit = {
    until(Working.cw()){
      val mode = io.StdIn.readLine("请输入工作模式 [1:新闻，2:普通，3:xx是什么梗]:")
      matchWorkingMode(mode)
    }
  }
  @scala.annotation.tailrec
  def until(cond: =>Boolean)(block: =>Unit):Unit = {
    block
    if(cond) until(cond)(block)
  }
  /**
   * @param mode:String
  **/
  def matchWorkingMode(mode:String):Unit = {
    mode match {
      case "1" => ArticleMakers.makeANewsArticle()
      case "2" => ArticleMakers.makeAGeneralArticle()
      case "3" => ArticleMakers.makeAGeneral2Article()
      case _ => Console.err.println("\n错误")
    }
  }
}

package articlemaker

object Matchers {
  /**
   * @param x:article
   **/
  def matchArticle(x:article):String = {
    x match {
      case General2(theme, nameOfArticle) =>
        s"文章信息:主题:$theme,作者:$nameOfArticle,种类:${General2.label}"
      case General(titleOf, nameOfArticle) =>
        s"文章信息:主题:$titleOf,作者:$nameOfArticle,种类:${General.label}"
      case News(title1, title2, nameOfArticle) =>
        s"文章信息:主题:${title1+title2}," +
        s"作者:$nameOfArticle," +
        s"种类:${News.label}"
    }
  }
}

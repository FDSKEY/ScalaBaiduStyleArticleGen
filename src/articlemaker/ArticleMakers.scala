package articlemaker
import articlemaker.Matchers.matchArticle
object ArticleMakers {
  final val MILLIS = util.Random.between(900,1300)
  def makeANewsArticle(): Unit = {
    val article = CreatNews.create(
      io.StdIn.readLine("输入东西"),
      io.StdIn.readLine("输入事情"),
      io.StdIn.readLine("输入名字"))
    matchAndPrint(article,MILLIS)
    println(article.toString)
	}
  def makeAGeneralArticle(): Unit = {
    val article = CreatGeneral.creat(
      io.StdIn.readLine("请输入主题"),
      io.StdIn.readLine("请输入名字"))
    matchAndPrint(article,MILLIS)
    println(article.toString)
	}
  def makeAGeneral2Article(): Unit = {
    val article = CreatGeneral2.creat(
      io.StdIn.readLine("请输入主题:"),
      io.StdIn.readLine("请输入名字："))
    matchAndPrint(article,MILLIS)
    println(article.toString)
	}
  def matchAndPrint(article:article, millis:Long): Unit = {
    println(s"文章信息：${matchArticle(article)}")
    println("正在生成...")
    Thread.sleep(millis)
  }
}

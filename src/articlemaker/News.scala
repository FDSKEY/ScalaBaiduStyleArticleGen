package articlemaker
/**
 * @param title1: String,
 * @param title2: String
 * @param nameOfArticle:String
 */
case class News private(title1: String, title2: String, nameOfArticle:String) extends article {
  override val name: String = nameOfArticle
  override val titleOfArticle: String = s"\n为什么${title1}会${title2}呢？结果令人震惊！"
  override val tamale: String = s"""
    |Hi,大家好，这里是$name。今天，我们来说一下${title1 + title2}
    |这件事。${title1 + title2}是怎么回事呢？${title1}相信大家都很熟悉，
    |但是${title1 + title2}这件事究竟是怎么回事呢？可能有人不理解，
    |为什么${title1}会${title2}呢？所以，下面就让小编带大家一起了解吧
    |${title1 + title2}这件事吧，${title1 + title2}其实就是${title1 + title2}了。
    |那么${title1}为什么会${title2}呢，相信大家都很好奇是怎么回事。大家可能会感到很惊讶，
    |${title1}怎么会${title2}呢？
    |
    |${title1 + title2}就是${title1 + title2}啦，这是因为${title1 + title2}就是${title1 + title2}
    |就是小编也感到非常惊讶。那么这就是关于${title1 + title2}的事情了，大家有没有觉得很神奇呢？
    |感谢大家阅读小编的文章，谢谢啦。
    |如果大家有什么想法和评论，欢迎在文章结尾下方留言。
  """.stripMargin
}
object News{
  val label: String = "新闻"
}
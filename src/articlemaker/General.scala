
package articlemaker

/**
 *@param title:String,
 *@param nameOfArticle:String
**/
case class General private(title:String, nameOfArticle:String) extends article {
  override val name: String = nameOfArticle
  override val titleOfArticle: String = s"${title}是怎么回事呢？结果令人震惊！"
  override val tamale: String = s"""
    |Hi,大家好，这里是$name。今天，我们来说一下$title
    |${title}是怎么回事呢？${title}相信大家都很熟悉， 但是${title}是怎么回事呢？
    |下面就让小编带大家一起了解吧。 $title，其实就是${title}了。
    |大家可能会感到很惊讶，${title}怎么是这样的呢？...
    |但事实就是这样，小编也感到非常惊讶。 那么这就是关于${title}的事情了，
    |大家有什么想法呢？欢迎在评论区告诉小编一起讨论哦
  """.stripMargin
}
object General{
  val label: String = "通用"
}
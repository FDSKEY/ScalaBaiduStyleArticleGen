package articlemaker

/**
 * @param theme:String
 * @param nameOfArticle:String
 **/
case class General2 private(theme:String, nameOfArticle:String) extends article {
  val name: String = nameOfArticle
  val titleOfArticle: String = s"${theme}是什么梗？今天终于水落石出！"
  val tamale: String =  s"""
    |Hi,大家好，这里是$name。今天我来说一下${theme}是什么梗？
    |${theme}是怎么一回事？${theme}是什么梗？${theme}最近为什么这么火呢？
    |大家都知道，${theme}最近很火，究竟是为什么很火呢？${theme}到底是什么梗？
    |相信大家对${theme}都很熟悉，${theme}就是我们每天都会经常遇到的，但是${theme}是怎么回事呢？
    |${theme}最近能火，其实就是${theme}受到了大家的关注。
    |大家可能会感到很惊讶，${theme}为什么是这样的？${theme}究竟为什么火起来了呢？
    |但事实就是这样，小编也感到非常惊讶。
    |希望小编精心整理的这篇内容能够帮助到你，本期教学结束了，
    |关于${theme}大家还有什么想说的吗？欢迎在评论区留言哦~
  """.stripMargin
}
object General2{
  val label: String = "通用2"
}
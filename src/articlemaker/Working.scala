package articlemaker
import io.StdIn._
object Working {
  /**
   * @param res:String = readLine("你要继续吗？[y/n]:")
   **/
  def cw(
    res:String = readLine("你要继续吗？[y/n]:")
  ) = res.toLowerCase match {
    case "y" => true
    case "n" => false
    case _ =>
      System.err.println("\n输入错误的值")
      false
  }
}
